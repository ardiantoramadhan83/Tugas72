import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';

const Home = ({navigation, route}) => {
  const {product} = useSelector(state => state.data);

  const dispatch = useDispatch();
  const onLogout = async () => {
    dispatch({type: 'LOGOUT'});
    navigation.replace('Login');
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.header}>
        <TouchableOpacity
          style={{
            width: '15%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={onLogout}>
          <Icon name="arrowleft" size={25} color="#fff" />
        </TouchableOpacity>
        <View
          style={{
            width: '70%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 16, fontWeight: '600', color: '#fff'}}>
            Home
          </Text>
        </View>
        <View style={{width: '15%'}} />
      </View>
      <FlatList
        data={product}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={styles.cardItem}
            onPress={() => navigation.navigate('AddData', {item})}>
            <View style={[styles.viewInfo, {marginTop: 0}]}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  ID
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View style={{width: '65%', paddingLeft: 10}}>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.id}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Nama Lengkap
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View style={{width: '65%', paddingLeft: 10}}>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.fullName}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Email
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View style={{width: '65%', paddingLeft: 10}}>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.email}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Alamat
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View style={{width: '65%', paddingLeft: 10}}>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.address}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Nomor Telepon
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View style={{width: '65%', paddingLeft: 10}}>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.phone}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnFloating}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={25} color="#fff" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  btnFloating: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewInfo: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  cardItem: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 15,
    borderRadius: 4,
    borderColor: '#dedede',
    borderWidth: 1,
    padding: 15,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'red',
    paddingVertical: 15,
  },
});

export default Home;
